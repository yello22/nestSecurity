import { Test, TestingModule } from '@nestjs/testing';
import { UserService } from './user.service';
import { User } from './entities/user.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';

describe('UserService', () => {
  let userService: UserService;
  let userRepository: Partial<Repository<User>>;

  beforeEach(async () => {
    userRepository = {
      create: jest.fn(),
      save: jest.fn(),
      find: jest.fn(),
      findOneBy: jest.fn(),
      preload: jest.fn(),
      remove: jest.fn(),
    };
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserService,
        {
          provide: getRepositoryToken(User),
          useValue: userRepository,
        },
      ],
    }).compile();

    userService = module.get<UserService>(UserService);
    userRepository = module.get<Repository<User>>(getRepositoryToken(User));
  });

  it('should be defined', () => {
    expect(userService).toBeDefined();
    expect(userRepository).toBeDefined();
  });

  it('should create new user', async () => {
    const userCreated = {
      email: 'razaro294@gmail.com',
    } as User;

    const candidateUser = {
      email: 'razaro294@gmail.com',
    } as CreateUserDto;

    jest.spyOn(userRepository, 'create').mockReturnValue(userCreated);
    jest.spyOn(userRepository, 'save').mockResolvedValue(userCreated);

    const result = await userService.create(candidateUser);

    expect(userRepository.create).toHaveBeenCalledWith(userCreated);
    expect(userRepository.save).toHaveBeenCalledWith(userCreated);
    expect(result.email).toEqual(userCreated.email);
  });
});
