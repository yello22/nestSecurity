import { Test, TestingModule } from '@nestjs/testing';
import { AuthenticationService } from './authentication.service';
import { UserService } from '../user/user.service';
import { AuthenticationController } from './authentication.controller';
import { ProtectedRequest } from 'src/request/request.interface';
import { UserDto } from 'src/user/dto/user.dto';
import { JwtService } from '@nestjs/jwt';
import { User } from 'src/user/entities/user.entity';

describe('AuthenticationController', () => {
  let authenticationService: Partial<AuthenticationService>;
  let userService: Partial<UserService>;
  let authenticationController: AuthenticationController;

  beforeEach(async () => {
    authenticationService = {
      validateUser: jest.fn(),
      alreadyHaveAccount: jest.fn(),
      googleLogin: jest.fn(),
      login: jest.fn(),
      signup: jest.fn(),
      loginWith2fa: jest.fn(),
      isTwoAuthenticationCodeValid: jest.fn(),
      generateTwoFactorAuthenticationSecret: jest.fn(),
      generateQrCodeDataUrl: jest.fn(),
    };

    userService = {
      create: jest.fn(),
      findAll: jest.fn(),
      findOne: jest.fn(),
      turnOnTwoFactorAuthentication: jest.fn(),
    };

    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthenticationController],
      providers: [
        {
          provide: AuthenticationService,
          useValue: authenticationService,
        },
        {
          provide: UserService,
          useValue: userService,
        },
        JwtService,
      ],
    }).compile();

    authenticationController = module.get<AuthenticationController>(
      AuthenticationController,
    );
    authenticationService = module.get<AuthenticationService>(
      AuthenticationService,
    );
    userService = module.get<UserService>(UserService);
  });

  it('should be defined', () => {
    expect(authenticationController).toBeDefined();
    expect(authenticationService).toBeDefined();
    expect(userService).toBeDefined();
  });

  describe('login', () => {
    it('should login user', async () => {
      const result = {
        email: 'razaro294@gmail.com',
        accessToken: 'mlkjMlkjmkAO315OMLkjmlkfqsdfq',
      };

      const request = {} as ProtectedRequest;

      jest.spyOn(authenticationService, 'login').mockResolvedValue(result);

      await expect(authenticationController.login(request)).resolves.toEqual(
        result,
      );
    });
  });

  describe('signup', () => {
    it('should signup user', async () => {
      const result = {
        email: 'razaro294@gmail.com',
        accessToken: 'mlkjMlkjmkAO315OMLkjmlkfqsdfq',
        password: 'MKLSDJG02135901MZLKGZL',
      };

      const request = {} as UserDto;

      jest.spyOn(authenticationService, 'signup').mockResolvedValue(result);

      await expect(authenticationController.signup(request)).resolves.toEqual(
        result,
      );
    });
  });

  describe('register', () => {
    const requestMock = {
      user: {
        id: 1,
        email: 'razaro294@gmail.com',
      },
    } as ProtectedRequest;

    const responseMock = {
      setHeader: jest.fn(),
      send: jest.fn(),
    };

    const authSecretMock = {
      secret: '1MKLTMJ2P5',
      otpAuthUrl: 'otpauth://totp/test@test.com?secret=secret',
    };

    const bufferMock = Buffer.from('buffer');

    beforeEach(() => {
      jest
        .spyOn(authenticationService, 'generateTwoFactorAuthenticationSecret')
        .mockResolvedValue(authSecretMock);

      jest
        .spyOn(authenticationService, 'generateQrCodeDataUrl')
        .mockResolvedValue(bufferMock);
    });
    it('should generate 2FA QR code', async () => {
      await authenticationController.register(requestMock, responseMock as any);

      expect(
        authenticationService.generateTwoFactorAuthenticationSecret,
      ).toHaveBeenCalledWith(requestMock.user);
      expect(authenticationService.generateQrCodeDataUrl).toHaveBeenCalledWith(
        authSecretMock.otpAuthUrl,
      );
      expect(responseMock.setHeader).toHaveBeenCalledWith(
        'Content-Type',
        'image/png',
      );
      expect(responseMock.send).toHaveBeenCalledWith(bufferMock);
    });
  });

  describe('turnOnTwoFactorAuthentication', () => {
    const requestMock = {
      user: {
        id: 1,
        email: 'razaro294@gmail.com',
      },
    } as ProtectedRequest;

    const userWith2faActicated = {
      id: 1,
      email: 'razaro294@gmail.com',
      isTwoFactorAuthenticationEnabled: true,
    } as User;

    const body = { twoFactofAuthenticationCode: '454976' };

    beforeEach(() => {
      jest
        .spyOn(authenticationService, 'isTwoAuthenticationCodeValid')
        .mockReturnValue(true);
      jest
        .spyOn(userService, 'turnOnTwoFactorAuthentication')
        .mockResolvedValue(userWith2faActicated);
    });

    it('should turnOnTwoFactorAuthentication', async () => {
      const result =
        await authenticationController.turnOnTwoFactorAuthentication(
          requestMock,
          body,
        );
      expect(result.isTwoFactorAuthenticationEnabled).toEqual(
        userWith2faActicated.isTwoFactorAuthenticationEnabled,
      );
    });
  });
});
